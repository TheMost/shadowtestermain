import soy
from time import sleep

room = soy.scenes.Room(soy.atoms.Size((30.0,30.0,30.0)))
client = soy.Client()
client.window.background = soy.atoms.Color('white')
client.window.title = "Four Mini Cubes"

gold = soy.atoms.Color('gold')
firebrick = soy.atoms.Color('firebrick')

cubemap = soy.textures.Cubemap("checkered", [gold, firebrick], 1,1,1)
room['cube'] = soy.bodies.Box()
room['cube'].position = soy.atoms.Position((0,0,-3))
room['cube'].material = soy.materials.Textured()
room['cube'].material.colormap = cubemap
room['cube'].size = soy.atoms.Size((6,6,6))
room['cube'].radius = 0.1

room['topcube'] = soy.bodies.Box()
room['topcube'].position = soy.atoms.Position((0,8,-2))
room['topcube'].material = soy.materials.Textured()
room['topcube'].material.colormap = cubemap
room['topcube'].size = soy.atoms.Size((3,3,3))
room['topcube'].radius = 0.1

room['bottomcube'] = soy.bodies.Box()
room['bottomcube'].position = soy.atoms.Position((0,-8,-2))
room['bottomcube'].material = soy.materials.Textured()
room['bottomcube'].material.colormap = cubemap
room['bottomcube'].size = soy.atoms.Size((3,3,3))
room['bottomcube'].radius = 0.1

room['leftCube'] = soy.bodies.Box()
room['leftCube'].position = soy.atoms.Position((-8,0,-2))
room['leftCube'].material = soy.materials.Textured()
room['leftCube'].material.colormap = cubemap
room['leftCube'].size = soy.atoms.Size((3,3,3))
room['leftCube'].radius = 0.1

room['rightCube'] = soy.bodies.Box()
room['rightCube'].position = soy.atoms.Position((8,0,-2))
room['rightCube'].material = soy.materials.Textured()
room['rightCube'].material.colormap = cubemap
room['rightCube'].size = soy.atoms.Size((3,3,3))
room['rightCube'].radius = 0.1

room['cam'] = soy.bodies.Camera(soy.atoms.Position((0, 0, 25)))
room['toplight'] = soy.bodies.Light(soy.atoms.Position((0, 15, 0)))
room['bottomlight'] = soy.bodies.Light(soy.atoms.Position((0, -15, 0)))
room['leftLight'] = soy.bodies.Light(soy.atoms.Position((-15, 0, 0)))
room['rightLight'] = soy.bodies.Light(soy.atoms.Position((15, 0, 0)))
client.window.append(soy.widgets.Projector(room['cam']))


# Events init
soy.events.KeyPress.init()
soy.events.KeyRelease.init()
soy.events.Motion.init()

# Forces
Rforce = soy.atoms.Vector((1000, 0, 0))		# Right force
Lforce = soy.atoms.Vector((-1000, 0, 0))    # Left force
Uforce = soy.atoms.Vector((0, 1000, 0))     # Up force
Dforce = soy.atoms.Vector((0, -1000, 0))    # Down force
Fforce = soy.atoms.Vector((0, 0, -2000))    # Forwards force
Bforce = soy.atoms.Vector((0, 0, 2000))	    # Backwards force

# Actions
RThrust = soy.actions.Thrust(room['cam'], Rforce)
LThrust = soy.actions.Thrust(room['cam'], Lforce)
UThrust = soy.actions.Thrust(room['cam'], Uforce)
DThrust = soy.actions.Thrust(room['cam'], Dforce)
FThrust = soy.actions.Thrust(room['cam'], Fforce)
BThrust = soy.actions.Thrust(room['cam'], Bforce)
LookAround = soy.actions.Look(room['cam'])

# Events
soy.events.Motion.addAction(LookAround)
soy.events.KeyPress.addAction("Right", RThrust)
soy.events.KeyRelease.addAction("Right", LThrust)
soy.events.KeyPress.addAction("Left", LThrust)
soy.events.KeyRelease.addAction("Left", RThrust)
soy.events.KeyPress.addAction("Up", UThrust)
soy.events.KeyRelease.addAction("Up", DThrust)
soy.events.KeyPress.addAction("Down", DThrust)
soy.events.KeyRelease.addAction("Down", UThrust)
soy.events.KeyPress.addAction("W", FThrust)
soy.events.KeyRelease.addAction("W", BThrust)
soy.events.KeyPress.addAction("S", BThrust)
soy.events.KeyRelease.addAction("S", FThrust)



if __name__ == '__main__' :
    while client.window :
        sleep(.1)
